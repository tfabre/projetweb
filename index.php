<?php
header("Location: ./view/home.php");
include_once("model/Configurations.class.php");
include_once("model/base/Film.class.php");
include_once("model/db/ObjectDB.class.php");
include_once("model/db/mysql/FilmDB.class.php");
include_once("model/db/mysql/MemberDB.class.php");
?>
<!DOCTYPE html>
<html>
  <head>
    <title>FILM</title>
    <meta charset="utf-8"/>
  </head>
  <body>
    <p>Projet Web...</p>
    <?php
    // NOTE: These are just tests... (you can delete as you want !)
    //~ $film = new Film(1, 'The Iron Giant', "Kirk Wise", "animated film", "2001-06-03");
    //~ print_r($film->toArray());
    echo("<p>===<br/></p>");
    $filmDB = new FilmDB();
    $a = $filmDB->getFilmsByBestRating();
    print_r($a);
    print_r($filmDB->getMemberRate('Roro', 1));
    //~ $filmDB->create("dd", "ddirector", "ddexplosion", "2000-02-20");
    //~ $filmDB->update(2, "AA", "Adirector", "Aanimation", "2000-02-02");
    $aa = $filmDB->search("The Iron Giant");
    echo("<br/>###<br/>");
    print_r($aa);
    echo("<br/>###<br/>");
    // $filmDB->delete(2);

//////////////////////////////////////////////////////////////////////////////////

    $memberDB = new MemberDB();
    if ($memberDB->usernameExists("Roro"))
        echo("<br/>Username Exists<br/>");
    else
        echo("<br/>Usernane doesn't exist<br/>");
    $status = $memberDB->identifyMember("Roro", "tot");
    if ($status == null)
        echo("Member doesn't exist<br/>");
    else
        echo("Member exists: ".$status."<br/>");

    // $memberDB->create("Riri", "duck");

    $status = $memberDB->identifyMember("Roro", "toto");
    if ($status == null)
        echo("Member doesn't exist<br/>");
    else
        echo("Member exists: ".$status."<br/>");
        
    $memberDB1 = new MemberDB();
    //~ $memberDB1->create("eiei", "kk", "admin");
    $status = $memberDB->identifyMember("eiei", "kk");
    if ($status == null)
        echo("Member doesn't exist<br/>");
    else
        echo("Member exists: ".$status."<br/>");

    $memberDB->mark("Roro", 1, 0);
    echo(Configurations::get("dsn")."<br/>");

    if (isset($_FILES["filmPoster"]) && $_FILES["filmPoster"]["error"] == 0)
        $filmDB->setImage(1, $_FILES["filmPoster"]);
    ?>





<!--
//////////////////         SIGNING UP/IN             //////////////////////////
-->
<script type="text/javascript">
function validationUp() {
    var username = document.getElementById("usernameUp").value;
    var password = document.getElementById("passwordUp").value;
    var confirmpassword = document.getElementById("confirmpasswordUp").value;
    var ok = true;
    if (password == "" || username == "") {
		alert("Please fill in the informations.");
		ok = false;
	}
    if (password != confirmpassword) {
        alert("Passwords do not match.");
        ok = false;
    }
    return ok;
}

function validationIn() {
    var username = document.getElementById("usernameIn").value;
    var password = document.getElementById("passwordIn").value;
    var ok = true;
    if (password == "" || username == "") {
		alert("Please fill in the informations.");
		ok = false;
	}
    return ok;
}
</script>

<h3>Sign up</h3>
	<form method="POST" action="controller/signup.php" onsubmit="return validationUp()">
		<label>Username :</label><br/>
		<input type=text name="username" id="usernameUp"/><br/>
		<label>Password : </label><br/>
		<input type=password name="password" id="passwordUp"/><br/>
		<label>Confirm password : </label><br/>
		<input type=password name="confirmpassword" id="confirmpasswordUp"/><br/>
		<input type=submit value="Sign up"/><br/>
	</form>

<h3>Sign in</h3>
	<form method="POST" action="controller/signin.php" onsubmit="return validationIn()">
		<label>Username :</label><br/>
		<input type=text name="username" id="usernameIn"/><br/>
		<label>Password : </label><br/>
		<input type=password name="password" id="passwordIn"/><br/>
		<input type=submit value="Sign in"/><br/>
	</form>
<!--
///////////////////////////////////////////////////////////////////////////////
-->

    <form action="index.php" method="post" enctype="multipart/form-data">
      <label for="file">Image à envoyer:</label><br/>
      <input type="file" name="filmPoster"/><br/>
      <input type="submit" value="envoyer"/>
    </form>

  </body>
</html>
