<?php

class Film
{
    private $id; //< int
    private $title; //< string
    private $director; //< string
    private $genre; //< string
    private $date; //< string
    private $rate; //< int
    private $img; //< string
    private $review; //< string
    private $synopsis; //< string

    /**
     * Construct a film
     * @param $id int The film database id
     * @param $title str The film title
     * @param $director str The film director
     * @param $genre str The film genre
     * @param $date str The realase date (format "Y-m-d")
     * @param $rate int The film avegerate rate
     * @param $img str The film poster
     */
    public function __construct($id, $title, $director, $genre, $date, $rate=-1,
                                $img="", $synopsis="")
    {
        $this->id = $id;
        $this->title = $title;
        $this->director = $director;
        $this->genre = $genre;
        $this->date = $date;
        $this->rate = $rate;
        $this->img = $img;
        $this->review = null;
        $this->synopsis = $synopsis;
    }

    /**
     * Set the Film id value
     * @param $id int The new id value
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set the Film title
     * @param $title str The new film title
     */
    public function setTitle($title)
    {
        $this->id = $title;
    }

    /**
     * Create an array with the attributs value.
     * @retval array An associative array which contains the attributes name and
     *               their values
     */
    public function toArray()
    {
        return array("id" => $this->id, "title" => $this->title,
                     "director" => $this->director, "genre" => $this->genre,
                     "date" => $this->date, "rating" => $this->rate,
                     "img" => $this->img, "review" => $this->review,
                     "synopsis" => $this->synopsis);
    }
}
