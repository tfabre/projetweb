 <?php

require_once(__DIR__."/../ObjectDB.class.php");
require_once(__DIR__."/../../Configurations.class.php");

class FilmDB extends ObjectDB
{
    /**
     * Create a new Film in database (with auto-increment id)
     * @param $title string The film's title
     * @param $director string The film's director
     * @param $genre string The film's genre
     * @param $date string The film's release date (format "Y-m-d")
     * @param $rate int The film's avegerate rate (default = -1 to show there is
     *                  note rate)
     * @param $img array The film poster file (give super global $_FILES
     *                   sub-array)
     */
    public function create($title, $director, $genre, $date, $rate=-1,
                           $img=null, $synopsis="")
    {
        // generate new id
        $stmt = $this->execQuery("SELECT MAX(idF) FROM Film");
        $id = $stmt->fetch();
        $id = $id[0] +1;
        // create the new entry in database
        $this->execQuery("INSERT INTO Film (idF, title, director, genre, ".
                         "releaseDate, avgRate, synopsis) VALUES (:id, :title,".
                         " :director, :genre, :date, :rate, :syno)",
                         array("id" => $id, "title" => $title,
                               "director" => $director, "genre" => $genre,
                               "date" => $date, "rate" => $rate, "syno" =>
                               $synopsis));
        if ($img != null)
            $this->setImage($id, $img);
    }

    /**
     * Store the poster path into the database, and download the image on the
     * server
     * @param $id int The film id
     * @param $img array The film poster file (give super global $_FILES
     *                    sub-array)
     */
    public function setImage($id, $img)
    {
        if ($img["error"] == 0)
        {
            if ($img["size"] < Configurations::get("upload_max_size", 64000000))
            {
                $extensions = explode(',', Configurations::get("upload_img_extension", array("jpg", "jpeg", "png")));

                $imgInfos = pathinfo($img["name"]);
                if (in_array(strtolower($imgInfos["extension"]), $extensions))
                {
                    $file = "/upload/film/img/".basename($img["name"]);
                    move_uploaded_file($img["tmp_name"],
                                       __DIR__."/../../..".$file);
                    chmod(__DIR__."/../../..".$file, 662);

                    $this->execQuery("UPDATE Film SET image = :img ".
                                     "WHERE idF = :id",
                                     array("img" => "/projetweb".$file,
                                           "id" => $id));
                }
            }
        }
    }

    /**
     * Delete a Film in the database
     * @param $id int The film identified this id
     */
    public function delete($id)
    {
        $this->execQuery("DELETE FROM Film WHERE idF = :id",array("id" => $id));
    }

    /**
     * Update film information in the database identified by id
     * @param $id int The film id to update
     * @param $title string The new film title
     * @param $director string The new film director
     * @param $genre string The new film genre
     * @param $date string The new film release date (premiere)
     */
    public function update($id, $title, $director, $genre, $date,$synopsis=null)
    {
        if ($synopsis == null)
            $this->execQuery("UPDATE Film SET title=:title,director=:director,".
                             "genre=:genre, releaseDate=:date WHERE idF = :id",
                             array("id" => $id, "title" => $title,
                                   "director" => $director, "genre" => $genre,
                                   "date" => $date));
        else
            $this->execQuery("UPDATE Film SET title=:title,director=:director,".
                             "genre=:genre, releaseDate=:date, synopsis=:syno ".
                             "WHERE idF = :id",
                             array("id" => $id, "title" => $title,
                                   "director" => $director, "genre" => $genre,
                                   "date" => $date, "syno" => $synopsis));
    }

    /**
     * Return an associative array with all the film stored in the database
     * @retval array The films store in the database
     */
    public function getFilms()
    {
        $stmt = $this->execQuery("SELECT * FROM Film");
        $ret = array();
        while ($res = $stmt->fetch())
            $ret[] = (new Film($res["idF"], $res["title"], $res["director"],
                               $res["genre"], $res["releaseDate"],
                               $res["avgRate"], $res["image"], $res["synopsis"])
                     )->toArray();
        return $ret;
    }

    /**
     * Return an array with the film order from the best mark to the worst
     * @retval array The array that contains the films ordered     
     */
    public function getFilmsByBestRating()
    {
        $stmt = $this->execQuery("SELECT * FROM Film ORDER BY avgRate DESC");
        $ret = array();
        while ($res = $stmt->fetch())
            $ret[] = (new Film($res["idF"], $res["title"], $res["director"],
                               $res["genre"], $res["releaseDate"],
                               $res["avgRate"], $res["image"], $res["synopsis"])
                      )->toArray();
        return $ret;
    }

    /**
     * Return an array with the film order from the worst mark to the best
     * @retval array The array that contains the films ordered     
     */
    public function getFilmsByWorstRating()
    {
        $stmt = $this->execQuery("SELECT * FROM Film ORDER BY avgRate");
        $ret = array();
        while ($res = $stmt->fetch())
            $ret[] = (new Film($res["idF"], $res["title"], $res["director"],
                               $res["genre"], $res["releaseDate"],
                               $res["avgRate"], $res["image"], $res["synopsis"])
                     )->toArray();
        return $ret;
    }

    /**
     * Search in the database the film with the title given into parameter and
     * return it
     * @param $title str The film title
     * @retval array The film that match with the title, null otherwise
     */
    public function search($title)
    {
        $title = explode(' ', $title);
        $where = '';
        for ($i = 0; $i < count($title); ++$i)
        {
            $where .= ' title LIKE ? OR';
            $title[$i] = '%'.$title[$i].'%';
        }
        $where = substr($where, 0, -3);

        $stmt = $this->execQuery("SELECT * FROM Film WHERE".$where,
                                 $title);

        $ret = array();
        while ($res = $stmt->fetch())
            $ret[] = (new Film($res["idF"], $res["title"], $res["director"],
                               $res["genre"], $res["releaseDate"],
                               $res["avgRate"], $res["image"], $res["synopsis"])
                      )->toArray();
        return $ret;
    }

    /**
     * Search in the database the film with the genre given into parameter and
     * return it
     * @param $genre str The film genre
     * @retval array The array that contains the films that match with the genre, null otherwise
     */
    public function searchGenre($genre)
    {
        $stmt = $this->execQuery("SELECT * FROM Film WHERE genre = :genre",
                                 array("genre" => $genre));
        $ret = array();
        while ($res = $stmt->fetch())
            $ret[] = (new Film($res["idF"], $res["title"], $res["director"],
                               $res["genre"], $res["releaseDate"],
                               $res["avgRate"], $res["image"], $res["synopsis"])
                     )->toArray();
        return $ret;
    }

    /**
     * Search in the database the film with the director given into parameter and
     * return it
     * @param $director str The film director
     * @retval array The array that contains the films that match with the director, null otherwise
     */
    public function searchDir($director)
    {
        $director = explode(' ', $director);
        $where = '';
        for ($i = 0; $i < count($director); ++$i)
        {
            $where .= " director LIKE ? OR";
            $director[$i] = '%'.$director[$i].'%';
        }
        $where = substr($where, 0, -3);
        
        $stmt = $this->execQuery("SELECT * FROM Film WHERE".$where,
                                 $director);
        $ret = array();
        while ($res = $stmt->fetch())
            $ret[] = (new Film($res["idF"], $res["title"], $res["director"],
                               $res["genre"], $res["releaseDate"],
                               $res["avgRate"], $res["image"], $res["synopsis"])
                     )->toArray();
        return $ret;
    }

    /**
     * Search in the database the film with the year given into parameter and
     * return it
     * @param $year str The film year
     * @retval array The array that contains the films that match with the year, null otherwise
     */
    public function searchYear($year)
    {
        $stmt = $this->execQuery("SELECT * FROM Film WHERE year(releaseDate) = :year",
                                 array("year" => $year));
        $ret = array();
        while ($res = $stmt->fetch())
            $ret[] = (new Film($res["idF"], $res["title"], $res["director"],
                               $res["genre"], $res["releaseDate"],
                               $res["avgRate"], $res["image"], $res["synopsis"])
                     )->toArray();
        return $ret;
    }

    /**
     * Search in the database the member rate for the film identified by $idF
     * @param $username str The member username
     * @param $idF int The film identifier
     * @retval int The rate given by the member. If the member doesn't rate the
     *             film, this function return -1
     */
    public function getMemberRate($username, $idF)
    {
        $stmt = $this->execQuery("SELECT note FROM Rating NATURAL JOIN Member ".
                                 "WHERE idF = :idF AND username = :uname",
                                 array("idF" => $idF, "uname" => $username));
        return ($res = $stmt->fetch()) ? $res["note"] : -1;
    }

    /**
     * Search in the database all the member's rate and store them in array
     * @param $idF int The film id
     * @retval array The collection of member's username and member's rate
     */
    public function getAllMemberRate($idF)
    {
        $stmt = $this->execQuery("SELECT username, note FROM Rating NATURAL ".
                                 "JOIN Member WHERE idF = :idF",
                                 array('idF' => $idF));
        $ret = array();
        while ($res = $stmt->fetch())
            $ret[] = array('username'=>$res['username'], 'rate'=>$res['note']);

        return $ret;
    }

    /**
     * Search in the database the member review for the film identified by $idF
     * @param $username str The member username
     * @param $idF int The film identifier
     * @retval str The review given by the member. If the member doesn't review the
     *             film, this function return null
     */
	public function getMemberReview($username, $idF)
	{
		$stmt = $this->execQuery("SELECT review FROM Review NATURAL JOIN Member ".
		                         "WHERE idF = :idF AND username = :uname",
		                         array("idF" => $idF, "uname" => $username));
		return ($res = $stmt->fetch()) ? $res["review"] : null;
	}

    /**
     * Search in the database all the members' reviews and store them in array
     * @param $idF int The film id
     * @retval array The collection of members' usernames and members' reviews
     */
    public function getAllMemberReview($idF)
    {
        $stmt = $this->execQuery("SELECT username, review FROM Review NATURAL ".
                                 "JOIN Member WHERE idF = :idF",
                                 array('idF' => $idF));
        $ret = array();
        while ($res = $stmt->fetch())
            $ret[] = array('username'=>$res['username'], 'review'=>$res['review']);

        return $ret;
    }

    /**
     * Search in the database for the film identified by id given in paramet
     * synopsis
     * @param $idF int The film id
     * @retval str|null Return the synopsis if it exists, null otherwise
     */
    public function getSynopsis($idF)
    {
        $stmt = $this->execQuery("SELECT synopsis FROM Film WHERE idF = :id",
                                 array("id" => $idF));
        return ($res = $stmt->fetch()) ? $res["synopsis"] : null;
    }
    
    /**
     * Set a synopsis or update it if it already exists to the film identified by
     * $idF
     * @param $idF int The film id
     * @param $synopsis str The synopsis
     */
    public function updateSynopsis($idF, $synopsis)
    {
        $stmt = $this->execQuery("UPDATE Film SET synopsis = :syno WHERE idF = :id",
                                 array("id" => $idF, "syno" => $synopsis));
    }
    
    public function getFilmById($idF)
    {
		$ret = null;
		$stmt = $this->execQuery("SELECT * FROM Film WHERE idF = :id",
								 array("id" => $idF));
								 
		if ($res = $stmt->fetch())
			$ret = (new Film($res["idF"], $res["title"], $res["director"],
                             $res["genre"], $res["releaseDate"],
                             $res["avgRate"], $res["image"], $res["synopsis"])
                             )->toArray();
        return $ret;
	}
}
