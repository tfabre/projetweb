<?php

require_once(__DIR__."/../ObjectDB.class.php");

class MemberDB extends ObjectDB
{
    /**
     * Create a new member on the database
     * @param $username str The username
     * @param $password str The non-hashed password
     * @param $status str The Member status (=> 'admin'|'member'), default is 
     *                    'member'
     */
    public function create($username, $password, $status='member')
    {
        // generate new id
        $stmt = $this->execQuery("SELECT MAX(idM) FROM Member");
        $id = $stmt->fetch();
        $id = $id[0] +1;
        // create the new entry in database
        $this->execQuery("INSERT INTO Member (idM, username, password, status)".
                         " VALUES (:id, :uname, :psw, :status)",
                         array("id" => $id, "uname" => $username,
                               "psw" => sha1($password), "status" => $status));

    }
    
    /**
     * Set a mark or update mark if it already exists to the film identified by
     * $idF
     * @param $username str The member username
     * @param $idF int The film id
     * @param $mark int The mark (between 0 and 5)
     */
    public function mark($username, $idF, $mark)
    {
        $idM = null;
        $stmt = $this->execQuery("SELECT idM FROM Member WHERE ".
                                 "username = :uname",
                                 array("uname" => $username));
        $idM = $stmt->fetch();
        if (isset($idM["idM"]))
        {
            $idM = $idM["idM"];

            $stmt = $this->execQuery("SELECT * FROM Rating WHERE idF = :idF ".
                                     "AND idM = :idM",
                                     array("idM" => $idM, "idF" => $idF));
            
            if ($stmt->fetch())
                $this->execQuery("UPDATE Rating SET note = :note WHERE ".
                                 "idM = :member AND idF = :film",
                                 array("member" => $idM, "film" => $idF,
                                       "note" => $mark));
            else
                $this->execQuery("INSERT INTO Rating (idM, idF, note) VALUES ".
                                 "(:member, :film, :note)",
                                 array("member" => $idM, "film" => $idF,
                                       "note" => $mark));
        }
    }

    /**
     * Set a review or update it if it already exists to the film identified by
     * $idF
     * @param $username str The member username
     * @param $idF int The film id
     * @param $mark str The review
     */
    public function comment($username, $idF, $review)
    {
        $idM = null;
        $stmt = $this->execQuery("SELECT idM FROM Member WHERE ".
                                 "username = :uname",
                                 array("uname" => $username));
                                 
        $idM = $stmt->fetch();
        if (isset($idM["idM"]))
        {
            $idM = $idM["idM"];

            $stmt = $this->execQuery("SELECT * FROM Review WHERE idF = :idF ".
                                     "AND idM = :idM",
                                     array("idM" => $idM, "idF" => $idF));
            
            if ($stmt->fetch())
                $this->execQuery("UPDATE Review SET review = :review WHERE ".
                                 "idM = :member AND idF = :film",
                                 array("member" => $idM, "film" => $idF,
                                       "review" => $review));
            else
                $this->execQuery("INSERT INTO Review (idM, idF, review) VALUES".
                                 " (:member, :film, :review)",
                                 array("member" => $idM, "film" => $idF,
                                       "review" => $review));
        }
    }

    /**
     * Indicate if the username is already used in database
     * @param $username string The username
     * @retval bool Return true if the username already exists, false otherwise
     */
    public function usernameExists($username)
    {
        $stmt = $this->execQuery("SELECT idM FROM Member WHERE username = :uname",
                                array("uname" => $username));
        return ($stmt->fetch()) ? true : false;
    }

    /**
     * Search in the database if the couple username/password exists, and return
     * the member status if so, null otherwise
     * @param $username string The username
     * @param $password string The non-hashed password
     * @retval string|null Return the member status if the member is identified,
     *                     null otherwise
     */
    public function identifyMember($username, $password)
    {
        $stmt=$this->execQuery("SELECT status FROM Member WHERE username = ".
                               ":uname AND password = :pass",
                               array("uname" => $username,
                                     "pass" => sha1($password)));
        $status = $stmt->fetch();
        return ($status == null) ? null : $status["status"];
    }
}
