<?php

require_once(__DIR__."/../Configurations.class.php");

abstract class ObjectDB
{
    private static $db;

    /**
     * Execute Query or Prepared statement and return the resulted statement
     * @param $sql string The sql instruction
     * @param $param array The associative array which contains the parameters
     *                     for prepared statement
     * @retval PDOStatement The result of the sql execution
     */
    protected function execQuery($sql, $param=null)
    {
        $stmt = null;
        if ($param == null)
        {
            try
            {
                $stmt = self::get_db()->query($sql);
            }
            catch (PDOException $e) { echo($e->getMessage()); }
        }
        else
        {
            try
            {
                $stmt = self::get_db()->prepare($sql);
                $stmt->execute($param);
            }
            catch (PDOException $e) { echo($e->getMessage()); }
        }
        return $stmt;
    }

    private static function get_db()
    {
        if (self::$db == null)
        {
            $dsn = Configurations::get("dsn");
            try
            {
                self::$db = new PDO($dsn, Configurations::get("db_username"),
                                    Configurations::get("db_password"));
            }
            catch (PDOException $e) { echo($e->getMessage()); }
            
            self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return self::$db;
    }
}