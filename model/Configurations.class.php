<?php

class Configurations
{
    private static $parameters;

    public static function get($attr, $defaultVal=null, $iniFile="")
    {
        return (isset(self::getParameters($iniFile)[$attr])) ?
            self::getParameters($iniFile)[$attr] : $defaultVal;
    }

    private static function getParameters($iniFile)
    {
        if (self::$parameters == null)
        {
            $file = __DIR__."/../config/dev.ini";
            if (file_exists($iniFile))
                $file = $iniFile;
            self::$parameters = parse_ini_file($file);
        }
        return self::$parameters;
    }
}
