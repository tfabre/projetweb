<?php
session_start();
include_once("../controller/admin.php");
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Admin page</title>
		<meta charset="utf-8"/>
		<link rel="stylesheet" type="text/css" href="css.css"/>
	</head>

<body>
<?php include("template-header.php"); ?>

<?php if(isset($_POST["add"])): ?>
<div class="div-admin">
	<h3>Add a new film</h3>
	<form method="POST" action="admin.php" enctype="multipart/form-data">
		<label>Title</label><br/>
		<input type=text name="title"/><br/>
		<label>Director</label><br/>
		<input type=text name="director"/><br/>
		<label>Genre</label><br/>
		<input type=text name="genre"/><br/>
		<label>Release date (YYYY-MM-DD)</label><br/>
		<input type=text name="date"/><br/>
		<input type="file" name="filmPoster"/><br/>
        <label>Synopsis:</label><br/>
        <textarea name="synopsis" rows="4" cols="40"></textarea><br/>
		<input type=submit name="add" value="Submit"/>
	</form>
</div>
<?php endif; ?>

<?php include("template-footer.php"); ?>
</body>

</html>

