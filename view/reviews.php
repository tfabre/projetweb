<?php include_once("../controller/reviews.php"); ?>

<!DOCTYPE html>
<html>
	<head>
		<title>Reviews</title>
		<meta charset="utf-8"/>
		<link rel="stylesheet" type="text/css" href="css.css"/>
	</head>

<body>
<?php include("template-header.php"); ?>
	<div class="div-listreviews">
		<?php if(isset($_GET["title"]) && isset($_GET["id"])): ?>
			<h4>Reviews for "<?php echo $_GET["title"]; ?>"</h4><br/>
			<?php foreach ($reviews as $res) { ?>
				<p><?= $res['username'] ?>:<br/>
				<?= $res['review'] ?>
				</p>
			<?php } // endfor; ?>
		<?php else: ?>
			<p>ERROR 400 Bad Request</p>
		<?php endif; ?>
	</div>

<?php include("template-footer.php"); ?>
</body>
</html>

