<?php
session_start();
include_once("../model/base/Film.class.php");
include_once("../model/Configurations.class.php");
include_once("../model/db/mysql/FilmDB.class.php");
include_once("../controller/voteRate.php");
include_once("../controller/updateSynopsis.php");

$filmDB = new FilmDB();

?>

<!DOCTYPE html>
<html>
	<head>
		<title>Film informations</title>
		<meta charset="utf-8"/>
		<link rel="stylesheet" type="text/css" href="css.css"/>
	</head>

<body>
<?php include("template-header.php"); ?>
	<div class="div-infosfilm">
		<?php if (isset($_POST["filmId"])): ?>
			<?php $film = $filmDB->getFilmById($_POST["filmId"]); ?>
			<img src="<?= Configurations::get("serv_root").$film["img"] ?>" class="img-film" alt="Poster coming soon"/>
			<p>
			<?= $film["title"]; ?><br/>
			<?= $film["director"]; ?><br/>
			<?= $film["genre"] ?><br/>
			<?= $film["date"] ?><br/>
			</p>
			<div class="div-synopsis">
			  <p>
				<?= $film["synopsis"] ?>
			  </p>
			  <?php if(isset($_POST["update-synopsis"])): ?>
				<form method="POST" action="film.php">
				  <?php if ($film["synopsis"] == null):?>
                    <input type="hidden" name="filmSynopsisId" value="<?= $_POST["filmId"] ?>"/>
                    <textarea name="synopsisTA" rows="4" cols="40" placeholder="Type the synopsis here."></textarea><br/>
				  <?php else: ?>
                    <input type="hidden" name="filmSynopsisId" value="<?= $_POST["filmId"] ?>"/>
                    <textarea name="synopsisTA" rows="4" cols="40"><?php echo $film["synopsis"]?></textarea><br/>
                  <?php endif; ?>
                   <input type="hidden" name="filmId" value="<?= $_POST["filmId"] ?>"/>
				   <p>
				     <input type="submit" name="send-update-synopsis" value="Submit"/>
				   </p>
				</form>
			  <?php endif; ?>
			  <?php if (isset($_SESSION["status"]) && $_SESSION["status"] == "admin"): ?>
				<form method="POST" action="film.php">
				  <p class="update-synopsis">
				   <input type="hidden" name="filmId" value="<?= $_POST["filmId"] ?>"/>
				   <input type="submit" name="update-synopsis" value="Update synopsis"/>
				  </p>
				</form>
			  <?php endif; ?>
			</div>
			
			<br/>
			
			<?php if ($film["rating"]<0): ?>
				<p>Be the first to rate this movie!</p>
			<?php else: ?>
				<p>Members' rating:</p>
				<?php
				 for ($i = 0; $i<5; ++$i) {
					if ($film["rating"]>$i)
					  echo '<img src="img/star.png" alt="&#9733;"/>';
					else
					  echo '<img src="img/black_star.png" alt="&#9734;"/>';
				  } ?>
			<?php endif; ?>
			
			<br/>

			<?php
				$filmDB = new FilmDB();
				if (isset($_SESSION["status"]) && ($_SESSION["status"] == "admin" || $_SESSION["status"] == "member")): ?>
			   <div>
				 <form method="POST" action="film.php">
				   <p>Vote:</p>
				   <p>
				   <?php if ($film["rating"] == -1):?>
						  <input type=checkbox name="vote" value="0" id="0" class="star" onChange="check(this.id)"/>
						  <label for="0" title="rate 0"></label>
						  <input type=checkbox name="vote" value="1" id="1" class="star" onChange="check(this.id)"/>
						  <label for="1" title="rate 1"></label>
						  <input type=checkbox name="vote" value="2" id="2" class="star" onChange="check(this.id)"/>
						  <label for="2" title="rate 2"></label>
						  <input type=checkbox name="vote" value="3" id="3" class="star" onChange="check(this.id)"/>
						  <label for="3" title="rate 3"></label>
						  <input type=checkbox name="vote" value="4" id="4" class="star" onChange="check(this.id)"/>
						  <label for="4" title="rate 4"></label>
						  <input type=checkbox name="vote" value="5" id="5" class="star" onChange="check(this.id)"/>
						  <label for="5" title="rate 5"></label>
					  <?php else: ?>
						  <?php
							$rate = $filmDB->getMemberRate($_SESSION["username"], $_POST['filmId']);
						  ?>
						  <input type=checkbox name="vote" value="0" id="0" class="star" onChange="check(this.id)" <?php if ($rate >= 0) echo("checked"); ?>/>
						  <label for="0" title="rate 0"></label>
						  <input type=checkbox name="vote" value="1" id="1" class="star" onChange="check(this.id)" <?php if ($rate >= 1) echo("checked"); ?>/>
						  <label for="1" title="rate 1"></label>
						  <input type=checkbox name="vote" value="2" id="2" class="star" onChange="check(this.id)" <?php if ($rate >= 2) echo("checked"); ?>/>
						  <label for="2" title="rate 2"></label>
						  <input type=checkbox name="vote" value="3" id="3" class="star" onChange="check(this.id)" <?php if ($rate >= 3) echo("checked"); ?>/>
						  <label for="3" title="rate 3"></label>
						  <input type=checkbox name="vote" value="4" id="4" class="star" onChange="check(this.id)" <?php if ($rate >= 4) echo("checked"); ?>/>
						  <label for="4" title="rate 4"></label>
						  <input type=checkbox name="vote" value="5" id="5" class="star" onChange="check(this.id)" <?php if ($rate >= 5) echo("checked"); ?>/>
						  <label for="5" title="rate 5"></label>
					  <?php endif; ?>
				   </p>
				   <input type="hidden" name="filmId" value="<?= $_POST["filmId"] ?>"/>
				   <input type="hidden" name="filmReviewId" value="<?= $_POST["filmId"] ?>"/>
				   <?php $review = $filmDB->getMemberReview($_SESSION["username"], $_POST["filmId"]);
					  if ($review == null):?>
					   <textarea name="reviewTA" rows="4" cols="30" placeholder="Type your review here."></textarea><br/>
				   <?php else: ?>
					  <textarea name="reviewTA" rows="4" cols="30"><?php echo $review?></textarea><br/>
				   <?php endif; ?>
				   <input type="submit" name="submit-review" value="Submit"/>
				 </form>
			   </div>
			<?php endif; ?>
		<?php else: ?>
			<p>ERROR 400 Bad Request</p>
		<?php endif; ?>
	</div>

<?php include("template-footer.php"); ?>
</body>

<script type="text/javascript">
      function check(id)
      {
		  var mark = id;
          for(var i = parseInt(mark)+1; i<=5; ++i)
              document.getElementById(i.toString()).checked = false;

          for(var i = parseInt(mark); i>=0; --i)
              document.getElementById(i.toString()).checked = true;
      }
</script>

</html>

