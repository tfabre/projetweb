<?php
session_start();

include_once("../model/Configurations.class.php");
include_once("../model/base/Film.class.php");
include_once("../model/db/mysql/FilmDB.class.php");
include_once("../controller/searchFilm.php");
include_once("../controller/searchFilmByBestRating.php");
include_once("../controller/searchFilmByWorstRating.php");
include_once("../controller/searchFilmByGenre.php");
include_once("../controller/searchFilmByDirector.php");
include_once("../controller/searchFilmByDate.php");
include_once("../controller/updatePosition.php");
include_once("../controller/admin.php");
include_once("../controller/vote.php");
include_once("../controller/voteRate.php");

$filmDB = new FilmDB();
if (!isset($films))
    $films = $filmDB->getFilms();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Home</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" type="text/css" href="css.css"/>
  </head>
  <body>
<?php include("template-header.php"); ?>

<?php
/*
 *  Si on est admin :
 *  La barre avec l'ajout de film apparait sous le header
 */
  if (isset($_SESSION["status"]) && $_SESSION["status"] == "admin"): ?>
    <section class="section-admin">
      <form action="admin.php" method="post">
        <p>Admin option:
          <input type="submit" name="add" value="Add film"/>
        </p>
      </form>
    </section>
<?php endif; ?>

	<section class="section-search">
		<form method="POST"  action="home.php" id="multi-search-form">
			<h3>Advanced search:</h3>
			<label>Search by:</label>
			<select name="searchOptions">
				<option value="bestRating">best rating</option>
				<option value="worstRating">worst rating</option>
				<option value="title">title</option>
				<option value="genre">genre</option>
				<option value="director">director</option>
				<option Value="date">release year</option>
			</select>
			<input type="text" name="advSearch" placeholder="Keywords"/>
			<input type="submit" value="Search" name="AdvSearch"/>
			 <img src="img/smloupe.png" height="30" width="30"> 
		</form>
	</section>

    <section class="section-film">
      <?php if (isset($ERRORS)): ?>
      <?= $ERRORS ?>
      <?php endif; ?>
      <?php foreach ($films as $film) { ?>
      <article>
        <div class="div-img-film">
				<form id="infos<?= $film["id"] ?>" method="POST" action="film.php">
					<input type=hidden name="filmId" value="<?= $film["id"] ?>"/>
					<input type=hidden name="title" value="<?= $film["title"] ?>"/>
					<input type=hidden name="director" value="<?= $film["director"] ?>"/>
					<input type=hidden name="genre" value="<?= $film["genre"] ?>"/>
					<input type=hidden name="date" value="<?= $film["date"] ?>"/>
					<input type=hidden name="synopsis" value="<?= $film["synopsis"] ?>"/>
					<input type=hidden name="rating" value="<?= $film["rating"] ?>"/>
					<input type=hidden name="poster" value="<?= Configurations::get("serv_root").$film["img"] ?>"/>
				</form>
		  <a href="#" class="link-img-film" onclick='document.getElementById("infos<?= $film["id"] ?>").submit()'>
		  <img src="<?= Configurations::get("serv_root").$film["img"] ?>" class="img-film" alt="Poster coming soon"/>
		  </a>
		</div>
        <?php if (isset($_SESSION["status"]) && $_SESSION["status"] == "admin" &&
                    isset($_POST["update"]) && isset($_POST["id"]) && $_POST["id"] == $film["id"]): ?>
        <div class="div-info-update">
          <form method="POST" action="home.php" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?= $film["id"] ?>"/>
            <input type="text" name="title" value="<?= $film["title"]; ?>"/><br/>
            <input type="text" name="director" value="<?= $film["director"]; ?>"/><br/>
            <input type="text" name="genre" value="<?= $film["genre"]; ?>"/><br/>
            <input type="text" name="date" value="<?= $film["date"]; ?>"/><br/>
            <input type="file" name="filmPoster"/><br/>
            <input type="submit" name="update_action" value="Update"/>
          </form>
        </div>
          <?php else: ?>
        <div class="div-info">
		  <p>
		    <?= $film["title"]; ?><br/>
		    <?= $film["director"]; ?><br/>
		    <?= $film["genre"] ?><br/>
		    <?= $film["date"] ?><br/>
		  </p>
		</div>
          <?php endif; ?>
          
		<?php if (isset($_SESSION["status"]) && ($_SESSION["status"] == "admin" || $_SESSION["status"] == "member") &&
                  (isset($_POST["rate-review"]) && isset($_POST["id"]) && $film["id"] == $_POST["id"]) || (isset($_POST["already-rate-review"]) && isset($_POST["id"]) && $film["id"] == $_POST["id"])): ?>
           <div class="div-rating-review">
             <form method="POST" action="home.php" class="form-rate-review">
               <?php $review = $filmDB->getMemberReview($_SESSION["username"], $film["id"]);
                  if ($review == null):?>
                   <input type="hidden" name="filmReviewId" value="<?= $film['id'] ?>"/>
                   <textarea name="reviewTA" rows="4" cols="30" placeholder="Type your review here."></textarea><br/>
               <?php else: ?>
                  <input type="hidden" name="filmReviewId" value="<?= $film['id'] ?>"/>
                  <textarea name="reviewTA" rows="4" cols="30"><?php echo $review?></textarea><br/>
               <?php endif; ?>
               <p>Vote:
               <?php $rate = $filmDB->getMemberRate($_SESSION["username"], $film["id"]);
                  if ($rate == -1)
                  {
                      include_once("../controller/displayVotingStarsNoVote.php");
                      echo(displayVotingStarsNoVote($film));
                  }
                  else
                  {
                      include_once("../controller/displayVotingStars.php");
                      echo(displayVotingStars($rate, $film));
                  }
               ?>
               </p>
               <input type="submit" name="submit-review" value="Submit"/>
             </form>
		   </div>
        <?php else: ?>
		<div class="div-rating">
		  <p class="p-div-rating">
		  <?php if ($film["rating"]<0): ?>
		    Be the first to rate this movie!
		  <?php else: ?>
		    Members' rating:<br/>
          <a href="ratings.php?id=<?= $film["id"] ?>&title=<?= $film["title"] ?>" class="not"><?php
             $rating=$film["rating"];
             for ($i = 0; $i<5; ++$i) {
                if ($rating >0)
                  echo '<img src="img/star.png" alt="&#9733;"/>';
                else
                  echo '<img src="img/black_star.png" alt="&#9734;"/>';
                --$rating;
              } ?>
          </a>
          <?php endif; ?>
          </p>
          <?php if (isset($_SESSION["status"]) && ($_SESSION["status"] == "admin" || $_SESSION["status"] == "member")): ?>
            <?php $rate = $filmDB->getMemberRate($_SESSION["username"], $film["id"]);
                  if ($rate == -1): ?>
            <p class="p-div-rating">Review and vote:</p>
            <form method="POST" action="home.php" class="form-rate-review">
              <input type="hidden" name="id" value="<?= $film["id"] ?>"/>
			  <input type="submit" name="rate-review" value="Go!"/>
            </form>
            <p class="p-div-rating">Or vote only:</p>
            <?php include("../controller/starsNoVote.php"); ?>
            <?php else: ?>
            <p class="p-div-rating">See your review:</p>
            <form method="POST" action="home.php" class="form-already-rate-review">
              <input type="hidden" name="id" value="<?= $film["id"] ?>"/>
			  <input type="submit" name="rate-review" value="Go!"/>
            </form>
            <p class="p-div-rating">Your Vote:</p>
            <?php include("../controller/starsVote.php"); ?>
            <?php endif; ?>
          <?php endif; ?>
		</div>
        <?php endif; ?>
        <?php if (isset($_SESSION["status"]) && $_SESSION["status"] == "admin"): ?>
        <div class="admin-film-options">
          <form method="POST" action="home.php">
            <input type=hidden name="id" value="<?php echo $film["id"] ?>"/>
			<input type=hidden name="title" value="<?php echo $film["title"] ?>"/>
			<input type=hidden name="director" value="<?php echo $film["director"] ?>"/>
			<input type=hidden name="genre" value="<?php echo $film["genre"] ?>"/>
			<input type=hidden name="date" value="<?php echo $film["date"] ?>"/>
		    <input type="submit" name="update" value="Update"/>
            <input type="submit" name="delete" value="Delete"/>
          </form>
        </div>
        <?php endif; ?>
	  </article>
      <?php } //endfor; ?>
    </section>

<?php include("template-footer.php"); ?>

    <script type="text/javascript">
      function check(id)
      {
          id = id.split('_');
          var prefix = id[0].toString();
          var mark = id[1];
          for(var i = parseInt(mark)+1; i<=5; ++i)
              document.getElementById(prefix+'_'+i.toString()).checked = false;

          for(var i=parseInt(mark); i>=0; --i)
              document.getElementById(prefix+'_'+i.toString()).checked = true;
      }
  </script>
  </body>
</html>

