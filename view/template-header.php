    <header id="site-header">
      <a href="./home.php" class="links">Home Page</a>
    </header>

    <nav id="nav-head-search">
	  <?php if(!isset($_SESSION["username"])): ?>
      <a href="connection.php" class="connection-link">Sign in / sign up</a><br/>
      <?php else: ?>
      <p id="connection-info">You are logged in as <?php echo $_SESSION["username"]; ?>.</p>
      <a href="../controller/logout.php" class="connection-link">Log out</a><br/>
      <?php endif; ?>
		<form method="POST" action="home.php" id="nav-search-form">
			<label>Title :</label>
			<input type="text" name="search"/>
			<input type="submit" name="SearchTitle" value="Search"/>
		</form>
	</nav>
