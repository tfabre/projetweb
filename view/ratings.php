<?php include_once("../controller/ratings.php"); ?>

<!DOCTYPE html>
<html>
	<head>
		<title>Ratings</title>
		<meta charset="utf-8"/>
		<link rel="stylesheet" type="text/css" href="css.css"/>
	</head>

<body>
<?php include("template-header.php"); ?>
	<div class="div-listratings">
		<?php if(isset($_GET["title"]) && isset($_GET["id"])): ?>
			<h4>Ratings for "<?php echo $_GET["title"]; ?>"</h4><br/>
			<?php for ($j = 0, $y = 0; $j < count($rates); ++$j) { //$rates as $res) { ?>
				<p>
					<?= $rates[$j]['username'] ?>: 
					<?php for ($i = 0; $i<5; ++$i) { ?>
						<?php if ($rates[$j]['rate']>$i): ?>
						  <img src="img/star.png" alt="&#9733;"/>
						<?php else: ?>
						  <img src="img/black_star.png" alt="&#9734;"/>
						<?php endif; ?>
					<?php } // endfor ?>
				 </p>
				 <p><?php if(isset($reviews[$y])) { echo $reviews[$y]['review']; ++$y; } ?></p>
			<?php } // endfor; ?>
		<?php else: ?>
			<p>ERROR 400 Bad Request</p>
		<?php endif; ?>
		<?php if (isset($ERRORS)) echo $ERRORS; ?>
	</div>

<?php include("template-footer.php"); ?>
</body>
</html>

