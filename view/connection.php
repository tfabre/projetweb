<?php
require_once("../controller/signin.php");
require_once("../controller/signup.php");
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Connection</title>
		<meta charset="utf-8"/>
		<link rel="stylesheet" type="text/css" href="css.css"/>
	</head>

<body id="body-connection">
	<div class="div-connection-box">
		<div class="div-connection-form">
          <h4 class="connection-title">Please, sign in :</h4>
			<form method="POST" action="connection.php" onsubmit="return validationIn()">
				<label for="username" class="connection-label">Username :</label><br/>
				<input type=text name="username" id="usernameIn"/><br/><br/>
				<label for="password" class="connection-label">Password : </label><br/>
				<input type=password name="password" id="passwordIn"/><br/><br/>
				<input type=submit name="signin" value="Sign in"/><br/><br/>
			</form>
	      <h4 class="connection-title">Or sign up :</h4>
			<form method="POST" action="connection.php" onsubmit="return validationUp()">
				<label for="username" class="connection-label">Username :</label><br/>
				<input type=text name="username" id="usernameUp"/><br/><br/>
				<label for="password" class="connection-label">Password : </label><br/>
				<input type=password name="password" id="passwordUp"/><br/><br/>
				<label for="confirmpassword" class="connection-label">Confirm password : </label><br/>
				<input type=password name="confirmpassword" id="confirmpasswordUp"/><br/><br/>
				<input type=submit name="signup" value="Sign up"/>
			</form>
		</div>
	</div>

<script type="text/javascript">
  function validationUp() {
      var username = document.getElementById("usernameUp").value;
      var password = document.getElementById("passwordUp").value;
      var confirmpassword = document.getElementById("confirmpasswordUp").value;
      var ok = true;
      if (password == "" || username == "") {
          alert("Please fill in the informations.");
          ok = false;
      }
      if (password != confirmpassword) {
          alert("Passwords do not match.");
          ok = false;
      }
      return ok;
  }

function validationIn() {
    var username = document.getElementById("usernameIn").value;
    var password = document.getElementById("passwordIn").value;
    var ok = true;
    if (password == "" || username == "") {
        alert("Please fill in the informations.");
        ok = false;
    }
    return ok;
}
</script>

	</body>
</html>
