DELIMITER /
CREATE TRIGGER filmRate
  AFTER INSERT ON Rating
    FOR EACH ROW 
BEGIN
    UPDATE Film SET avgRate = (SELECT AVG(note) FROM Rating WHERE idF = NEW.idF)
        WHERE idF = NEW.idF;
END;
/

CREATE TRIGGER filmRateUpdate
  AFTER UPDATE ON Rating
    FOR EACH ROW 
BEGIN
    UPDATE Film SET avgRate = (SELECT AVG(note) FROM Rating WHERE idF = NEW.idF)
        WHERE idF = NEW.idF;
END;
/

DELIMITER ;
