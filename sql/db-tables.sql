CREATE TABLE Film
(      idF         INT PRIMARY KEY,
       title       VARCHAR(50) NOT NULL,
       director    VARCHAR(30),
       genre       VARCHAR(30),
       releaseDate DATE,
       avgRate     INT DEFAULT -1,
       image       VARCHAR(100),
       synopsis    TEXT);

CREATE TABLE Member
(      idM         INT PRIMARY KEY,
       username    VARCHAR(20) NOT NULL,
       password    VARCHAR(40) NOT NULL,
       status      VARCHAR(20) DEFAULT 'member',
       CONSTRAINT UniqueUName UNIQUE(username));

CREATE TABLE Rating
(      idF INT,
       idM INT,
       note INT NOT NULL,
       CONSTRAINT PKRating      PRIMARY KEY (idF, idM),
       CONSTRAINT FKFilm        FOREIGN KEY (idF)       REFERENCES Film(idF)      ON DELETE CASCADE,
       CONSTRAINT FKMember      FOREIGN KEY (idM)       REFERENCES Member(idM)    ON DELETE CASCADE);

CREATE TABLE Review
(      idF INT,
       idM INT,
       review TEXT DEFAULT NULL,
       CONSTRAINT PKReview      PRIMARY KEY (idF, idM),
       CONSTRAINT FKMovie       FOREIGN KEY (idF)       REFERENCES Film(idF)      ON DELETE CASCADE,
       CONSTRAINT FKUser        FOREIGN KEY (idM)       REFERENCES Member(idM)    ON DELETE CASCADE);

COMMIT;
