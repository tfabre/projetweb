<?php

include_once("../model/db/ObjectDB.class.php");
include_once("../model/db/mysql/MemberDB.class.php");

if(isset($_POST["signup"]) && !empty($_POST["username"]) && !empty($_POST["password"]))
{
	$username = $_POST["username"];
	$password = $_POST["password"];
	$memberDB = new MemberDB();
	if(!$memberDB->usernameExists(htmlspecialchars($username)))
	{
		$memberDB->create(htmlspecialchars($username), $password);
		echo '<script type="text/javascript">alert("The account has been successfully created.");</script>';
	} else
	{
		echo '<script type="text/javascript">alert("This username is already taken.");</script>';
	}
}

?>
