<?php

include_once("../model/db/mysql/FilmDB.class.php");

if(isset($_POST["send-update-synopsis"]) && isset($_POST["synopsisTA"]) && isset($_POST["filmSynopsisId"]))
{
	$filmDB = new FilmDB();

	$filmDB->updateSynopsis($_POST["filmSynopsisId"], htmlspecialchars($_POST["synopsisTA"]));

	echo '<script type="text/javascript">alert("Your synopsis is: \"'.$_POST["synopsisTA"].'\"");</script>';
}
