<?php

include_once("../model/db/ObjectDB.class.php");
include_once("../model/db/mysql/MemberDB.class.php");

if(isset($_POST["signin"]) && !empty($_POST["username"]) && !empty($_POST["password"]))
{
    $memberDB = new MemberDB();
	$status = $memberDB->identifyMember(htmlspecialchars($_POST["username"]),$_POST["password"]);
    if(is_null($status))
    {
        echo '<script type="text/javascript">alert("Invalid username or password.");</script>';
    } else
	{
        session_start();
        $_SESSION["username"] = htmlspecialchars($_POST["username"]);
        $_SESSION["status"] = $status;
        header("Location: ../view/home.php");
	}
}

?>
