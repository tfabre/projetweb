<?php
include_once("../model/db/mysql/FilmDB.class.php");

// Create
if (!empty($_SESSION["username"]) && $_SESSION["status"] == "admin" && isset($_POST["add"]) && isset($_POST["title"]) && isset($_POST["director"]) && isset($_POST["genre"]) && isset($_POST["date"]))
{
    $filmDB = new FilmDB();
    if (isset($_FILES["filmPoster"]) && $_FILES["filmPoster"]["error"] == 0 && isset($_POST["synopsis"]))
        $filmDB->create(htmlspecialchars($_POST["title"]), htmlspecialchars($_POST["director"]), htmlspecialchars($_POST["genre"]), htmlspecialchars($_POST["date"]), -1, $_FILES["filmPoster"], htmlspecialchars($_POST["synopsis"]));
    elseif (isset($_FILES["filmPoster"]) && $_FILES["filmPoster"]["error"] == 0)
        $filmDB->create(htmlspecialchars($_POST["title"]), htmlspecialchars($_POST["director"]), htmlspecialchars($_POST["genre"]), htmlspecialchars($_POST["date"]), -1, $_FILES["filmPoster"]);
    elseif (isset($_POST["synopsis"]))
        $filmDB->create(htmlspecialchars($_POST["title"]), htmlspecialchars($_POST["director"]), htmlspecialchars($_POST["genre"]), htmlspecialchars($_POST["date"]), -1, null, htmlspecialchars($_POST["synopsis"]));
    else
        $filmDB->create(htmlspecialchars($_POST["title"]), htmlspecialchars($_POST["director"]), htmlspecialchars($_POST["genre"]), htmlspecialchars($_POST["date"]));
	$_POST["add"] = "add";
    header("Location: home.php");
}

// Delete
if (isset($_POST["delete"]) && isset($_POST["id"]) && !empty($_SESSION["username"]) && $_SESSION["status"] == "admin")
{
	$filmDB = new FilmDB();
	$filmDB->delete($_POST["id"]);
	unset($_POST["id"]);
	$_POST["delete"] = "delete";
}

// Update
if (isset($_POST["update_action"]) && !empty($_SESSION["username"]) && $_SESSION["status"] == "admin" && isset($_POST["id"]) && isset($_POST["title"]) && isset($_POST["director"]) && isset($_POST["genre"]) && isset($_POST["date"]))
{
    $filmDB = new FilmDB();
	$filmDB->update(htmlspecialchars($_POST["id"]), htmlspecialchars($_POST["title"]), htmlspecialchars($_POST["director"]), htmlspecialchars($_POST["genre"]), htmlspecialchars($_POST["date"]));

    if (isset($_FILES["filmPoster"]) && $_FILES["filmPoster"]["error"] == 0)
        $filmDB->setImage($_POST["id"], $_FILES["filmPoster"]);
    
}

?>

