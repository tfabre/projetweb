<?php

include_once("../model/base/Film.class.php");
include_once("../model/db/mysql/FilmDB.class.php");

if (!isset($filmDB))
    $filmDB = new FilmDB();

if  (isset($_POST["AdvSearch"]) && isset($_POST["searchOptions"]) && ($_POST["searchOptions"])=="date" && isset($_POST["advSearch"]))
{
	$films = $filmDB->searchYear($_POST["advSearch"]);
    
    if (empty($films))
        $ERRORS = "<br/>Sorry, no result matches your query. Try again with different keywords.<br/><br/>";
}
?>
