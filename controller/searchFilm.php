<?php

include_once("../model/base/Film.class.php");
include_once("../model/db/mysql/FilmDB.class.php");

if (!isset($filmDB))
    $filmDB = new FilmDB();
    
if ((isset($_POST["search"]) && $_POST["search"] == "") || (isset($_POST["advSearch"]) && $_POST["advSearch"] == ""))
{
	$ERRORS = "<br/>Sorry, no result matches your query. Try again with different keywords.<br/><br/>";
    $films = array();
}
elseif (isset($_POST["SearchTitle"]) && isset($_POST["search"]))
{
	$films = $filmDB->search($_POST["search"]);
    
    if (empty($films))
    {
       $ERRORS = "<br/>Sorry, no result matches your query. Try again with different keywords.<br/><br/>";
       $films = array();
    }
}

elseif (isset($_POST["AdvSearch"]) && isset($_POST["searchOptions"]) && ($_POST["searchOptions"])=="title" && isset($_POST["advSearch"]))
{
	$films = $filmDB->search($_POST["advSearch"]);
    
    if (empty($films))
    {
        $ERRORS = "<br/>Sorry, no result matches your query. Try again with different keywords.<br/><br/>";
        $films = array();
	}
}
?>
