<?php

include_once("../model/db/mysql/MemberDB.class.php");

//Vote and review
if(isset($_POST["submit-review"]) && !empty($_SESSION["username"]) && isset($_POST["vote"]) && isset($_POST["reviewTA"])&& !empty($_POST["reviewTA"]) && isset($_POST["filmId"]))
{
	$memberDB = new MemberDB();

	$memberDB->mark($_SESSION["username"], $_POST["filmId"], $_POST["vote"]);
	$memberDB->comment($_SESSION["username"], $_POST["filmId"], htmlspecialchars($_POST["reviewTA"]));

	echo '<script type="text/javascript">alert("Voted '.$_POST["vote"].'. Your review is: \"'.$_POST["reviewTA"].'.\"");</script>';
}

//Vote only
else if(isset($_POST["submit-review"]) && !empty($_SESSION["username"]) && isset($_POST["vote"]) && isset($_POST["filmId"]))
{
	$memberDB = new MemberDB();
	$memberDB->mark($_SESSION["username"], $_POST["filmId"], $_POST["vote"]);

	echo '<script type="text/javascript">alert("Voted '.$_POST["vote"].'.");</script>';
}

//Review only
else if(isset($_POST["submit-review"]) && !empty($_SESSION["username"]) && isset($_POST["reviewTA"]) && isset($_POST["filmReviewId"]))
{
	$memberDB = new MemberDB();

	$memberDB->comment($_SESSION["username"], $_POST["filmReviewId"], htmlspecialchars($_POST["reviewTA"]));

	echo '<script type="text/javascript">alert("Your review is: \"'.$_POST["reviewTA"].'\"");</script>';
}

?>
