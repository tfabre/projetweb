<?php

include_once("../model/db/mysql/MemberDB.class.php");

if(isset($_POST["rate"]) && !empty($_SESSION["username"]) && isset($_POST["vote"]) && isset($_POST["filmId"]))
{
	$memberDB = new MemberDB();
	$memberDB->mark($_SESSION["username"], $_POST["filmId"], $_POST["vote"]);


	echo '<script type="text/javascript">alert("Voted '.$_POST["vote"].'.");</script>';
}

?>
