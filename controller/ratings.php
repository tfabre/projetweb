<?php
session_start();

if(isset($_GET["title"]) && isset($_GET["id"]))
{
	include_once("../model/db/mysql/FilmDB.class.php");

	$filmDB = new FilmDB();
	$rates = $filmDB->getAllMemberRate($_GET["id"]);
	if(empty($rates))
	{
		$ERRORS = "No rating has been submitted for this film.";
	}
	$reviews = $filmDB->getAllMemberReview($_GET["id"]);
}

?>
