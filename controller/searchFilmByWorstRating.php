<?php

include_once("../model/base/Film.class.php");
include_once("../model/db/mysql/FilmDB.class.php");

if (!isset($filmDB))
    $filmDB = new FilmDB();

if (isset($_POST["AdvSearch"]) && isset($_POST["searchOptions"]) && ($_POST["searchOptions"])=="worstRating")
{
    $films = $filmDB->getFilmsByWorstRating();

if (empty($films))
$ERRORS = "<br/>Sorry, WORST RATING no result matches your query. Try again with different keywords.<br/><br/>";
}

?>
