<?php

require_once(__DIR__."/../model/db/mysql/FilmDB.class.php");

if ((isset($_SESSION["status"]) && $_SESSION["status"] == "admin" && isset($_POST["update"]) && isset($_POST["id"])) ||
	(isset($_SESSION["status"]) && ($_SESSION["status"] == "admin" || $_SESSION["status"] == "member") && isset($_POST["rate-review"]) && isset($_POST["id"])))
{
    $filmDB = new FilmDB();
    $films = $filmDB->getFilms();
    for ($i = 0; $i < count($films); ++$i)
    {
        if ($films[$i]["id"] == $_POST["id"])
        {
            $tmp = $films[$i];
            unset($films[$i]);
            array_unshift($films, $tmp);
            break;
        }
    }
}
